from boto.s3.connection import S3Connection
from optparse import OptionParser
import requests,json
import re
import sys
import urllib.request

parser = OptionParser()
parser.add_option("-u", "--username", dest="username",help="Enter the username")
parser.add_option("-p","--password", dest="password",help="Enter the password")
parser.add_option("-x","--hhax", dest="hhax_username",help="y/n based on creating admin user")

(options, args) = parser.parse_args()

if not options.username:   # if Username is not given
    parser.error('Username not given!')

if not options.password:   # if Password is not given
    parser.error('Password not given!')

#if not re.match(r'[A-Za-z0-9@#$%^&+=?,!_]{8,}', options.password):
#    parser.error('Password do not match the compliance rule')

masterpass = 'Cr!31n@l'

#if not options.hhax_username or options.hhax_username == 'y' :   # if filename is not given
#    users = [{'username' : options.username , 'password' : options.password},
#            {'username' : options.username+"_hhax" , 'password' : masterpass}]
#elif options.hhax_username == 'n':
users = [{'username' : options.username , 'password' : options.password}]

username = 'AKIAIMAN3TERBN75W72A'
password = '0TeAZzOxQTNcrJrarn5Xall02srE1iZCFsMFx+Yg'	
jusername = 'pgandhi'
jpassword = ''
url = 'https://jscape01.hhaexchange.com:11880/rest/management/'
url2 = 'https://jscape02.hhaexchange.com:11880/rest/management/'
verify_cert = False
#print(urllib.request.urlopen(url2).getcode())
#sys.exit()

#make the connection to AWS s3
conn = S3Connection(username, password)

#connect the bucket
mybucket = conn.get_bucket('hhaxsftp')

#jscape login
logindata =  '''{
  "username": "'''+jusername+'''",
  "password": "'''+jpassword+'''"
}'''
json_data = json.loads(logindata)
response = requests.post(url + "login/",json=json_data, verify=verify_cert)

cookieget = response.cookies
sessionid = cookieget['JSESSIONID_11880']
domain = 'hhaexchange'
cookie = {'JSESSIONID_11880': sessionid}
#sys.exit()

#list of the folders
folder = {
    'inbox':'/Inbox/',
    'trash':'/Inbox/Trash/',
    'inbox_remittance':'/Inbox/Remittance/',
    'outbox':'/Outbox/',
    'processed':'/Processed/',
    'failed':'/Processed/Failed/',
    'save':'/Save/'}

path_folder = {
    'main': {'path':'','permission' : {
        "fileDownloadingAllowed": True,
        "fileUploadingAllowed": True,
        "fileDeletionAllowed": True,
        "fileAppendingAllowed": True,
        "fileListingAllowed": True,
        "fileRenamingAllowed": True,
        "directoriesListingAllowed": True,
        "directoryMakingAllowed": True,
        "directoryDeletionAllowed": False,
        "subdirectoriesBrowsingAllowed": True
    }},
    'inbox': {
        'path' :'/Inbox/',
        'permission' : {
            "fileDownloadingAllowed": True,
            "fileUploadingAllowed": True,
            "fileDeletionAllowed": True,
            "fileAppendingAllowed": False,
            "fileListingAllowed": True,
            "fileRenamingAllowed": True,
            "directoriesListingAllowed": True,
            "directoryMakingAllowed": False,
            "directoryDeletionAllowed": False,
            "subdirectoriesBrowsingAllowed": True
        }},
    'outbox':{
        'path' :'/Outbox/',
        'permission' : {
            "fileDownloadingAllowed": True,
            "fileUploadingAllowed": True,
            "fileDeletionAllowed": False,
            "fileAppendingAllowed": False,
            "fileListingAllowed": True,
            "fileRenamingAllowed": True,
            "directoriesListingAllowed": True,
            "directoryMakingAllowed": False,
            "directoryDeletionAllowed": False,
            "subdirectoriesBrowsingAllowed": True
        }},
    'processed':{
        'path' :'/Processed/',
        'permission' : {
            "fileDownloadingAllowed": True,
            "fileUploadingAllowed": True,
            "fileDeletionAllowed": False,
            "fileAppendingAllowed": False,
            "fileListingAllowed": True,
            "fileRenamingAllowed": True,
            "directoriesListingAllowed": True,
            "directoryMakingAllowed": False,
            "directoryDeletionAllowed": False,
            "subdirectoriesBrowsingAllowed": True
        }},
    'save':{
        'path' :'/Save/',
        'permission' : {
            "fileDownloadingAllowed": True,
            "fileUploadingAllowed": True,
            "fileDeletionAllowed": False,
            "fileAppendingAllowed": False,
            "fileListingAllowed": True,
            "fileRenamingAllowed": True,
            "directoriesListingAllowed": True,
            "directoryMakingAllowed": False,
            "directoryDeletionAllowed": False,
            "subdirectoriesBrowsingAllowed": True
        }}}

path = []

#create folder in S3
mybucket.new_key('/'+users[0]["username"]+'/').set_contents_from_string('')
for key, value in folder.items():
    mybucket.new_key(users[0]["username"] + value).set_contents_from_string('')

#create reverse proxy
for key, value in path_folder.items():
    data =  {
          "protocol": "AmazonS3",
          "name": users[0]["username"]+"_"+key,
          "encryptionRequired": False,
          "username": username,
          "password": password,
          "usingCredentials": False,
          "remoteDirectory": {
            "@type": "REMOTE_PATH",
            "path": "hhaxsftp/"+users[0]["username"]+value["path"]
          },
          "logDirectory": None,
          "tags": [],
          "version": 1,
          "timeToLive": None,
          "host": ""
        }

    json_rpdata = json.loads(json.dumps(data))
    response_rp = requests.post(url + "reverse-proxies/"+domain,json=json_rpdata,cookies=cookie, verify=verify_cert)
    print(response_rp)
    #sys.exit()

    #path for creating the user
    path.append({
        "name": users[0]["username"]+"_"+key,
        "path": value["path"],
        "accessPermissions": value["permission"],
        "secured": True,
        "denied": False,
        "indexable": False,
        "reverseProxy": users[0]["username"]+"_"+key,
    })

for user in users:

    #create user
    userdata = {
    	"username": user["username"],
    	"login": user["username"],
        "password": user["password"],
    	"emailAddress": "",
    	"company": None,
    	"phone": {
    		"code": "",
    		"number": "",
    		"extension": ""
    	},
    	"owner": None,
    	"expirationDate": None,
    	"enabled": True,
    	"secured": True,
    	"groups": [],
    	"bindedKeys": [],
    	"resources": path,
    	"passwordChangingAllowed": True,
    	"emailFileTransferAllowed": True,
    	"usePhoneAuthentication": False,
    	"ignorePasswordAgingRules": False,
    	"quotas": {
    		"uploadsQuota": None,
    		"downloadsQuota": None,
    		"transfersQuota": None,
    		"maxUploadsPerSession": None,
    		"maxDownloadsPerSession": None,
    		"maxTransferRate": None
    	},
    	"ipAccessVerifier": {
    		"masks": []
    	},
    	"administration": None,
    	"webOptions": {
    		"loginRedirection": {
    			"target": "storage"
    		},
    		"webAccountLinkAvailable": True,
    		"webPersonalInformationAvailable": True,
    		"webPublicKeyAuthenticationAvailable": True,
    		"webOpenPgpEncryptionAvailable": True,
    		"webQuotasAvailable": True,
    		"webContactsAvailable": True,
    		"webAdHocActivityAvailable": True,
    		"webDropZonesAvailable": True,
    		"webAftpProtocolPreferable": False
    	},
    	"tags": [],
    	"uploadsQuota": None,
    	"downloadsQuota": None,
    	"transfersQuota": None,
    	"maxUploadsPerSession": None,
    	"maxDownloadsPerSession": None,
    	"maxTransferRate": None,
    	"loginRedirection": {
    		"target": "storage"
    	},
    	"webPersonalInformationAvailable": True,
    	"webPublicKeyAuthenticationAvailable": True,
    	"webQuotasAvailable": True
    }
    json_userdata = json.dumps(userdata)
    response_user = requests.post(url + "accounts/"+domain,json=userdata,cookies=cookie, verify=verify_cert)
    print('qqq')
    print(response_user)

#logout
response_logout = requests.post(url + "logout/",cookies=cookie, verify=verify_cert)
print(response_logout)
print("Done")